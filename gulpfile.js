var elixir = require('laravel-elixir'),
resourceFolder = 'ram_resources';
WPDestinationPath = '';
cssFolder = 'css';
jsFolder = 'js';
fontsFolder = 'fonts';

elixir((mix) => {
    mix
    .sass(['app.scss'], WPDestinationPath + cssFolder + '/ram.css', resourceFolder + '/sass/')
});
